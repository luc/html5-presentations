% Owncloud
% Luc Didry
% 27 mars 2014

# Divers

## Possibilités d'accès

* web (<http://cloud.ciril.fr/>) ;
* montage webdav (webdav://cloud.ciril.fr/remote.php/webdav/) ;
* synchronisation de dossiers directement dans l'OS (<http://owncloud.org/install/>).

## Authentification

![](img/login.png)

------------

Authentification via LDAP, accessible uniquement au groupe réseau

## Dossiers et fichiers

![Capture d'écran partielle de la liste des fichiers et dossiers](img/shared_folders.png)

# Avantages par rapport à Samba

## Accessibilité

* accessible depuis le web ;
* synchronisation de dossiers modifiés hors connexion (permet le travail hors-ligne) ;
* clients pour les téléphones.

## Confidentialité

* possibilité de création de partage par personne, par groupe ;
* on n'est pas obligés de partager tous ses documents ;
* possibilité de création de partages publics (pour communiquer un gros document par ex.).

## Flux d'activité

![](img/activity.png)

* permet de savoir quand quelqu'un partage quelque chose ;
* accessible via flux RSS ;

------------

L'application d'activités, en développement actif, permettra de :

* recevoir les activités par mail ;
* configurer finement le flux d'activité.

#Erreurs

## 
Gaia est relativement mal rangée, rendant la recherche de documents assez difficile.

* il faudra revoir le système de classement ;
* la recherche intégrée à Owncloud pourra sans doute nous aider !

------------

![](img/zatsole.svg)
