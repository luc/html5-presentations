# <small>Une conférence</small><br><span class="frama">Frama</span><span class="soft">soft</span><br> <span class="big bg">![mascotte Framasoft](img/manchot.png)</span>

-----------

Framasoft est un réseau d'<span class="orange">éducation populaire</span> consacré principalement au <span class="orange">logiciel et à la culture libre</span>

<small>épaulé par une association  
sans but lucratif</small>

-----------

La philosophie des logiciels libres peut se définir  
en trois mots

-----------

> - Liberté
> - Égalité
> - Fraternité

![Stallman en Panoramix](img/stallmanix.png)
<div class="notes">
- la liberté d'exécuter le programme comme vous voulez, pour n'importe quel usage (liberté 0) ;
- la liberté d'étudier le fonctionnement du programme, et de le modifier pour qu'il effectue vos tâches informatiques comme vous le souhaitez (liberté 1) ; l'accès au code source est une condition nécessaire ;
- la liberté de redistribuer des copies, donc d'aider votre voisin (liberté 2) ;
- la liberté de distribuer aux autres des copies de vos versions modifiées (liberté 3) ; en faisant cela, vous donnez à toute la communauté une possibilité de profiter de vos changements ; l'accès au code source est une condition nécessaire.
</div>
-----------

Les logiciels libres, de par leur nature ouverte, sont les seuls à vraiment garantir le respect de votre vie privée.

<small>Par exemple, Adobe, avec son logiciel [Digital Editions](https://www.actualitte.com/usages/adobe-avoue-espionner-ses-utilisateurs-une-affaire-rootkit-en-vue-53069.htm)  
vous espionne joyeusement.</small>

-----------

Mais le problème n'est plus que dans les ordinateurs.

# Google, Apple, Facebook, Amazon, Microsoft…

-----------

Tant de services web, si pratiques…

-----------

- qui nous espionnent ;
- exploitent nos données personnelles ;
- contrôlent nos appareils ;
- jouent avec nos pensées !

![Cyberpolice](img/googlus.png)


# Espionnage <br> ![NSA](img/nsa.png)

-----------

Ces géants américains sont soumis au Patrioct Act et sont tenus de fournir à la NSA tout ce qu'elle demande

-----------

En 2013, [Edward Snowden](https://fr.wikipedia.org/wiki/Edward_Snowden) fait fuiter des documents, dévoilant de telles pratiques

![Edward Snowden](img/snowden.jpg)

-----------

En 2015, avec le projet de loi sur le renseignement, le gouvernement français veut faire la même chose que la NSA !

<div class="notes">
- boîtes noires
- algorithmes de détection de comportement
    - chez les FAI
    - chez les GAFAM
- pas d'intervention de juges
- « contrôle » a posteriori et plutôt ardu
</div>

-----------

[Renseignez-vous](http://standblog.org/blog/post/2015/03/27/Loi-Renseignement-revue-de-presse) et n'hésitez pas à interpeller votre député !

# Exploitation de vos données personnelles <br> <span class="bg">![Faut fait du pognon](img/pognon.png)</span>

-----------

Généralement, si c'est gratuit, c'est vous le produit

-----------

[Google scanne vos mails](https://fr.wikipedia.org/wiki/Gmail#Publicit.C3.A9)

-----------

Google [analyse](https://www.google.fr/intl/fr/policies/terms/regional.html) votre comportement et vos goûts pour [personnaliser la publicité](https://www.google.fr/intl/fr/policies/technologies/ads/)

-----------

Facebook va aussi personnaliser la [publicité affichée](https://fr.wikipedia.org/wiki/Critiques_de_Facebook)

-----------

Google se sert de vos avis dans [ses pubs](http://www.huffingtonpost.fr/2013/10/13/google-photos-publicite-profils--desactiver-option_n_4092163.html)

-----------

Et je ne vous parle pas de tous [les droits qu'ils détiennent sur ce que vous avez mis chez eux](https://www.facebook.com/legal/terms) !

# Contrôle des appareils <br> ![Steve Jobs](img/steve.png)

-----------

Vous ne pouvez pas installer sur un iPhone d'application non validée par Apple

-----------

Amazon a déjà [supprimé des ebooks des Kindles](https://fr.wikipedia.org/wiki/Amazon_Kindle#L.27Affaire_1984) (liseuses) de ses clients

# Contrôle de la pensée <br> ![Mind control](img/mindcontrol.jpg)

-----------

Deux personnes n'auront pas les mêmes réponses à une même recherche sur Google

-----------

Facebook a admis avoir manipulé le mur de certaines personnes dans le cadre d'une [expérience comportementale](http://www.numerama.com/magazine/29852-facebook-a-teste-sa-capacite-de-manipulation-mentale-des-foules.html)

# La solution ?

-----------

Utiliser des services basés sur des logiciels libres et auto-hébergés

-----------

<div class="big bg">
![Tout le monde n'est pas assez geek pour le faire](img/pas_assez_geek.png)
</div>

-----------

<div class="big bg">
![Mais nous si](img/mais_nous_si.png)
</div>

# <span class="big"> ![Carte dégooglisons](img/carte.jpg) </span>

-----------

<span class="frama">**Frama**</span><span class="soft">**soft**</span> propose déjà un certain nombre d'alternatives libres à des services privateurs

-----------

[Framapad](https://framapad.org) en lieu et place de Google Docs

<div class="big">
![Framapad](img/framapad.png)
</div>

<div class="notes">
- Parler des instances temporaires
- Près de 1000 nouveaux pads tous les jours
</div>

-----------

[Framacalc](https://framacalc.org) propose la même chose mais sous la forme d'un tableur

<div class="big">
![Framacalc](img/framacalc.png)
</div>

<div class="notes">
Rappeler qu'il est en beta pour cause de bugs
</div>

-----------

Pourquoi utiliser Doodle quand il y a [Framadate](https://framadate.org) ?

<div class="big">
![Framadate](img/framadate.png)
</div>

-----------

Quant à Facebook… [Framasphere\*](https://framasphere.org) !

<div class="big">
![Framasphere](img/framasphere.png)
</div>

-----------

Vite fait, un petit dessin avec [Framavectoriel](http://framavectoriel.org)

<div class="big">
![Framavectoriel](img/framavectoriel.png)
</div>

-----------

Oubliez Imgur avec [Framapic](https://framapic.org)

<div class="big">
![Framapic](img/framapic.png)
</div>

<div class="notes">
- chiffré
- possibilité de suppression automatique
</div>

-----------

Raccourcissez vos URLs avec [Framalink/Huitre](https://frama.link)

<div class="big">
![Framalink/Huitre](img/framalink.png)
</div>

<div class="notes">
Expliquer le délire avec l'huitre
</div>

-----------

Transmettez des messages en toute sécurité avec [Framabin](https://framabin.org)

<div class="big">
![Framabin](img/framabin.png)
</div>

<div class="notes">
- chiffré
- commentaires
- expiration
</div>

-----------

On a même un méta-moteur de recherche avec [Framabee](https://framabee.org) !

<div class="big">
![Framabee](img/framabee.png)
</div>

<div class="notes">
- expliquer le principe d'un méta-moteur
- en béta
</div>

-----------

Nous avons aussi une [forge logicielle](https://git.framasoft.org) accessible à tous

<div class="big">
![Framagit](img/framagit.png)
</div>

<div class="notes">
Expliquer pourquoi il faut aussi se méfier de Github
</div>

-----------

Et bien d'autres encore ([présents ou à venir](http://degooglisons-internet.org/liste/))

<div class="notes">
- Framindmap
- Framabag
- Framanews
- Pétitions
- Framatalk
- Framadrop
- Framadrive
- Framaslides
</div>

# <span class="big bg">![Framasoft va devenir le prochain google alors ?](img/googlenext.png)</span>

-----------

Ce n'est pas notre but (et ça ne le sera jamais)

-----------

Vous pouvez installer chacun de nos logiciels chez vous et devenir votre propre fournisseur de services

-----------

Pour vous aider, nous nous engageons à mettre en ligne des [tutoriels d'installation](http://framacloud.org/cultiver-son-jardin/) pour chacun des services que nous proposons

<div class="notes">
Nouvelle Donne a installé son pod Diaspora\*
</div>

-----------

Nous avons une [charte](http://soutenir.framasoft.org/nav/html/cgu.html) nous engageant sur une vision éthique de nos services

-----------

Nous n'avons aucun but lucratif et aucun actionnaire à rémunérer

-----------

Les services <span class="frama">**Frama**</span><span class="soft">**soft**</span> sont gratuits et respectueux de la vie privée des utilisateurs

-----------

Alors, qui paye, si ce n'est pas nous le produit ?

-----------

Nous nous reposons sur l'économie du don, ainsi une partie des utilisateurs [financent nos services](https://soutenir.framasoft.org) qui profitent à tous

-----------

[Nous vendons aussi quelques *goodies*](http://enventelibre.org)

(tshirt, autocollants, etc)

-----------

Nous hébergeons aussi, contre rémunération, certains services pour d'autres

<div class="notes">
- nous avons de l'expertise sur nos services
- nous nous occupons de tout : installation, mise à jour, backup, conseil
</div>

# <span class="big bg">![Centralisation des données chez Framasoft ?](img/troll1.png)</span>

-----------

<span class="big bg">![Autre service centralisé](img/troll2.png)</span>

# Questions ?

-----------

## Merci d'être venus :-) <br> ![Chatons](img/cats.png)

-----------

Pour revoir cette présentation, rendez-vous sur <https://slides.fiat-tux.fr>

-----------

## Liens présents dans la présentation

- <https://www.actualitte.com/usages/adobe-avoue-espionner-ses-utilisateurs-une-affaire-rootkit-en-vue-53069.htm>
- <https://fr.wikipedia.org/wiki/Edward_Snowden>
- <http://standblog.org/blog/post/2015/03/27/Loi-Renseignement-revue-de-presse>
- <https://fr.wikipedia.org/wiki/Gmail#Publicit.C3.A9>

-----------

- <https://www.google.fr/intl/fr/policies/terms/regional.html>
- <https://www.google.fr/intl/fr/policies/technologies/ads/>
- <https://fr.wikipedia.org/wiki/Critiques_de_Facebook>
- <http://www.huffingtonpost.fr/2013/10/13/google-photos-publicite-profils--desactiver-option_n_4092163.html>

-----------

- <https://www.facebook.com/legal/terms>
- <https://fr.wikipedia.org/wiki/Amazon_Kindle#L.27Affaire_1984>
- <http://www.numerama.com/magazine/29852-facebook-a-teste-sa-capacite-de-manipulation-mentale-des-foules.html>
- <https://framapad.org>

-----------

- <https://framacalc.org>
- <https://framadate.org>
- <https://framasphere.org>
- <http://framavectoriel.org>

-----------

- <https://framapic.org>
- <https://frama.link>
- <https://framabin.org>
- <https://framabee.org>

-----------

- <https://git.framasoft.org>
- <http://degooglisons-internet.org/liste/>
- <http://framacloud.org/cultiver-son-jardin/>
- <http://soutenir.framasoft.org/nav/html/cgu.html>

-----------

- <https://soutenir.framasoft.org>
- <http://enventelibre.org>
