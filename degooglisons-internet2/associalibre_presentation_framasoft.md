# <small>Une conférence</small><br><span class="frama">Frama</span><span class="soft">soft</span><br> <span class="big bg">![mascotte Framasoft](img/manchot.png)</span>

# Qu'est-ce que Framasoft ?

-----------

Framasoft est un réseau d'<span class="orange">éducation populaire</span> consacré principalement au <span class="orange">logiciel et à la culture libre</span>

épaulé par une association sans but lucratif

# Définitions

-----------

<h2>Éducation populaire</h2>

-----------

Courant de pensée qui cherche principalement à promouvoir, en dehors des structures traditionnelles d'enseignement et des systèmes éducatifs institutionnels, une éducation visant l'amélioration du système social

source : [Wikipédia](https://fr.wikipedia.org/wiki/%C3%89ducation_populaire)

-----------

<h2>Logiciels libres</h2>

-----------

La philosophie des logiciels libres peut se définir  
en trois mots

-----------

* Liberté
* Égalité
* Fraternité

<div class="notes">
- la liberté d'exécuter le programme comme vous voulez, pour n'importe quel usage (liberté 0) ;
- la liberté d'étudier le fonctionnement du programme, et de le modifier pour qu'il effectue vos tâches informatiques comme vous le souhaitez (liberté 1) ; l'accès au code source est une condition nécessaire ;
- la liberté de redistribuer des copies, donc d'aider votre voisin (liberté 2) ;
- la liberté de distribuer aux autres des copies de vos versions modifiées (liberté 3) ; en faisant cela, vous donnez à toute la communauté une possibilité de profiter de vos changements ; l'accès au code source est une condition nécessaire.
</div>

-----------

<h2>Culture libre</h2>

-----------

La culture libre est un mouvement social qui promeut la liberté de distribuer et de modifier des œuvres de l'esprit sous la forme d'œuvres libres.

source : [Wikipédia](https://fr.wikipedia.org/wiki/Culture_libre)

# Historique <br>(dates marquantes)

-----------

2001 : naissance de Framasoft, annuaire de logiciels libres

-----------

2004 : création de l'association Framasoft et naissance du forum

-----------

2005 : création de la Framakey (applications portables)

-----------

2006 : 1er livre Framabook, création du Framablog et du groupe de traduction Framalang

-----------

2009 : lancement officiel de Framatube, création du FramaDVD et d'une Framakey Ubuntu

-----------

2010 : création de Framapack

-----------

2011 : création de Framapad et de Framadate

-----------

2012 : 4 nouveaux projets et une Framakey Wikipedia

-----------

2014 : lancement de la campagne « Dégooglisons Internet »

-----------

Et bien évidemment, je n'ai pas tout mis !

# Framablog

-----------

Ce blog traite de l'actualité du logiciel libre, de la culture libre ainsi que du réseau Framasoft 

-----------

Articles traduits, interviews, prises de position…

-----------

Accessible au néophyte comme au geek le plus barbu, Framablog s'adresse à tout le monde.

-----------

S'il ne traite pas des dernières sorties de logiciels, il n'en est pas moins un excellent outil de veille !

# Framabook

-----------

Une maison d'édition libre <span class="icon-emo-happy"></span>

-----------

À ce jour, 26 livres sont déjà sortis chez Framabook (d'autres livres sont en cours).

>- des romans
>- des manuels
>- des bandes dessinées
>- une biographie

-----------

Framabook se distingue particulièrement des autres maisons d'éditions par :

>- une contrainte forte : les livres doivent être diffusés sous licence libre
>- ses contrats qui laissent la possibilité à l'auteur de se faire éditer ailleurs s'il le souhaite
>- une marge plus importante pour les auteurs
>- une véritable collaboration entre les auteurs et les bénévoles du comité de lecture et du comité éditorial

-----------

Si les éditions papier sont payantes, les versions électroniques sont librement téléchargeables

# Framalang

-----------

Exclusivement composé de bénévoles, ce groupe de travail est d'une efficacité redoutable

-----------

S'il fournit régulièrement des articles traduits de l'anglais au Framablog, certains livres de la collection Framabook ont bénéficié de sa force de frappe

# Les services

-----------

Les premiers services visaient à simplifier l'utilisation de logiciels libres sur les ordinateurs

>- annuaire de logiciels
>- applications portables
>- pack d'applications pour une installation simplifiée

-----------

Puis, les usages évoluant, des services en ligne ont commencé à pointer le bout de leur nez…
<div class="fragment">
jusqu'à devenir plus que majoritaires !
</div>

-----------

Déjà plus 12 services vous tendent les bras…
<div class="fragment">
et si tout va bien, 7 autres les rejoindront d'ici la fin de l'année.
</div>

-----------

8 de plus en 2016 et encore 5 autres en 2017

-----------

Le grand nombre de services en ligne prévus correspond à la campagne [Dégooglisons Internet](http://degooglisons-internet.org).

# Financement des actions de Framasoft

-----------

« Si c'est gratuit, c'est toi le produit »

-----------

Chez Framasoft, c'est gratuit, mais ce sont les utilisateurs qui nous financent.

-----------

Nous nous reposons presque exclusivement sur l'économie du don

-----------

Ce sont donc les donateurs qui financent la gratuité de nos actions

# Des questions ?

# Merci d'être venus <span class="icon-emo-happy"></span> <br> ![CC-BY-SA Julián Ortega Martínez](img/cats3.png)

-----------

Pour revoir cette présentation, rendez-vous sur <https://slides.fiat-tux.fr>  
© 2015 Luc Didry  
Licence [CC-0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) 
