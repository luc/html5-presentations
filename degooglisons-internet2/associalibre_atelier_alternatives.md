# <small>Un atelier</small><br><span class="frama">Frama</span><span class="soft">soft</span><br> <span class="big bg">![mascotte Framasoft](img/manchot.png)</span>

# Google Doc vs Framapad & Framacalc

-------------

## Framapad

<div class="big">
![](img/framapad.png)
</div>

-------------

[Framapad](https://framapad.org) est une instance d'[Etherpad](http://etherpad.org)  
un éditeur de texte collaboratif en ligne

<div class="notes">
* expliquer différence éditeur / traitement de texte
* expliquer l'aspect collaboratif
</div>

-------------

Particularités de Framapad

>- simple
>- convivial
>- anonyme
>- temporaire ou non

<div class="notes">
* exemple des traductions
* exemple des réunions
</div>

-------------

Un plugin est en cours de développement pour intégrer un système de comptes permettant d'avoir des pads privés, d'être capable de les supprimer, etc.

-------------

Pour tester, rendez-vous sur  
<https://hebdo.framapad.org/p/bac-a-sable>

-------------

## Framacalc

<div class="big">
![](img/framacalc.png)
</div>

-------------

C'est la même chose, mais avec un tableur

-------------

Attention, les fonctionnalités sont moins nombreuses qu'avec un vrai tableur.

<div class="fragment">
Mais cela suffit **largement** pour un grand nombre de besoins.
</div>
<div class="fragment">
Le planning d'Associalibre a été fait sur un Ethercalc
</div>

-------------

Pour tester, rendez-vous sur  
<https://framacalc.org/bac-a-sable>

-------------

## Et pour l'ordi ?

-------------

[LibreOffice](http://www.libreoffice.org/) est une suite office libre, utilisant un format de document libre, pouvant cependant fonctionner avec des formats propriétaires.

-------------

<div class="big">
![Capture d'écran de LibreOffice](img/libreoffice_screenshot.png)
</div>

<div class="notes">
* Combien de fois avez-vous reçu un document que votre vieille version de Word ne pouvait ouvrir ?
* Un format libre et documenté permet de s'affranchir de ce genre de problèmes.
* Parler des problèmes d'import de documents propriétaires
* Parler des incompatibilités de certaines fonctionnalités avancées
</div>

# Gmail vs les autres

-------------

S'il est un point à ne pas négliger,  
c'est bien ce point névralgique de la communication.

<div class="notes">
Gmail scanne les mails…  
Est-ce vraiment quelque chose d'acceptable, surtout pour une association ?
</div>

-------------

Des fournisseurs alternatifs de mail existent,  
payants ou non.

-------------

Citons [OpenMailBox](https://www.openmailbox.org/), [MixMail](https://mixmail.fr/), [Mailoo](https://www.mailoo.org/), [Riseup](https://help.riseup.net/fr), [Sud-Ouest.org](https://www.sud-ouest.org/)…

-------------

Ces plateformes sont attachées à la vie privée de leurs utilisateurs, contrairement à Google et Cie  
et vous n'aurez pas non plus de publicité.

-------------

Il est parfois nécessaire d'adhérer à l'association pour pouvoir utiliser le service.

-------------

Certains, comme [MixMail](https://mixmail.fr/), proposent une offre pro,  
permettant d'utiliser votre propre nom de domaine.

-------------

L'idéal est toutefois d'héberger soi-même ses mails,  
éventuellement en payant un prestataire si vous n'avez pas les ressources en interne.  
[IndieHosters](https://indiehosters.net/applications/email.html) propose un tel service.

-------------

Mais toujours en utilisant des logiciels libres ! <span class="icon-emo-wink"></span>

<div class="notes">
L'offre mail d'OVH avec Exchange est donc plutôt à éviter.
</div>

-------------

Pour tester, rendez-vous sur  
<https://mixmail.fr/inscription/>

# Le moteur de recherche de Google vs les autres

-------------

Il existe des moteurs de recherche qui, à défaut d'être libre,  
respectent votre vie privée.

-------------

Par exemple, [Duckduckgo](https://duckduckgo.com) ou [Qwant](https://qwant.com)

<div class="notes">
* l'un est américain, l'autre est européen
</div>

-------------

Framasoft propose [Framabee](https://framabee.org),  
un méta-moteur de recherche libre

<div class="big">
![](img/framabee.png)
</div>

<div class="notes">
* expliquer que c'est en béta pour l'instant
* expliquer ce qu'est un méta moteur
</div>

-------------

Ajouter un moteur de recherche à son navigateur se fait en quelques clics !

-------------

<div class="big">
![](img/searchengine_add.png)
</div>

-------------

<div class="big">
![](img/searchengine_choose.png)
</div>

-------------

Je vous invite à faire la même recherche dans tous ces moteurs de recherche pour voir à quel point les résultats sont différents.

* <https://duckduckgo.com>
* <https://qwant.com>
* <https://framabee.org>
* <https://google.com>

# Doodle vs Framadate

-------------

Framasoft propose [Framadate](https://framadate.org), un service libre développé à partir de [Studs](https://sourcesup.cru.fr/projects/studs/)
<div class="big">
![](img/framadate.png)
</div>

<div class="notes">
* expliquer l'avantage du libre en parlant du fork
</div>

-------------

Framadate propose nativement certaines des fonctionnalités payantes de Doodle

<div class="notes">
* chiffrement intégral
* pas de publicité
* personnalisation intégrale… en l'installant et en le customisant
</div>

-------------

Pour tester, rendez-vous sur  
<https://framadate.org>

# Dropbox vs les autres

-------------

<div class="big">
![Il n'y a pas de cloud, ce sont juste les ordis d'autres personnes](img/no_cloud.png)
</div>

<div class="notes">
Savoir qu'il y a des contrats qui se trimballent sur les serveurs d'entreprises américaines, ça me fait froid dans le dos.
</div>

-------------

Il existe plusieurs solutions libres :

* [Owncloud](https://owncloud.org/)
* [Pydio](https://pyd.io/)
* [Seafile](http://seafile.com/en/home/)

-------------

Client de synchronisation, partage entre utilisateurs, partage public, groupes…  
Les fonctionnalités ne manquent pas !

<div class="notes">
Le développement est actif, les plugins sont nombreux
</div>

-------------

Il y moins de structures proposant ce genre de services car cela est coûteux en espace de stockage.

-------------

Notons tout de même la [mère Zaclys](http://gazette.zaclys.com/)

# ![Questions ?](img/questions.jpg)

-----------

Retrouvez ces alternatives et d'autres sur <https://wiki.framasoft.org/degooglisons-alternatives>.

-----------

Retrouvez ces slides sur <https://slides.fiat-tux.fr>.  
© 2015 Luc Didry  
Licence [CC-0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) 
