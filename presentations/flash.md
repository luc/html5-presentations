% Flash talk
% Luc Didry
% 14 juin 2014

# Inotify-hookable

[cpan App::Inotify::Hookable](https://metacpan.org/pod/App::Inotify::Hookable)

------------------

# Carton

[cpan Carton](https://metacpan.org/pod/Carton)

------------------

# Etherpad plugin: MyPads

[Campagne Ulule](http://ulule.com/etherpad-framapad/)

------------------

